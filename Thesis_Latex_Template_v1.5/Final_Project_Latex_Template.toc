\contentsline {chapter}{Acknowledgments}{iii}{section*.1}
\contentsline {chapter}{Abstract}{vii}{section*.2}
\contentsline {chapter}{Table of Contents}{ix}{section*.3}
\contentsline {chapter}{List of Figures}{xiii}{section*.5}
\contentsline {chapter}{List of Tables}{xv}{section*.7}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Project Goal}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Project Plan and Required Resources}{2}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Project Scope}{2}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Project Timeline}{2}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Costs}{3}{subsection.1.2.3}
\contentsline {chapter}{\numberline {2}Background and Related Work}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}IoT - The Internet of Things}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}DoS and DDoS - Denial of Service Attacks}{5}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Denial of service and Distributed Denial of Service}{5}{subsection.2.2.1}
\contentsline {subsubsection}{Type of Attacks}{5}{section*.10}
\contentsline {subsubsection}{Attack Methods for DDoS}{6}{section*.11}
\contentsline {subsubsection}{Attack Techniques}{6}{section*.12}
\contentsline {paragraph}{SYN Flood}{7}{section*.14}
\contentsline {paragraph}{UDP Flood}{8}{section*.15}
\contentsline {section}{\numberline {2.3}Attack Detection Methods}{8}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}k-Nearest Neighbours}{8}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Support Vector Machines}{8}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Decision tree}{9}{subsection.2.3.3}
\contentsline {subsubsection}{Random Forest}{9}{section*.16}
\contentsline {subsection}{\numberline {2.3.4}Neural Network Models - Multi-layer Perception}{9}{subsection.2.3.4}
\contentsline {subsection}{\numberline {2.3.5}ADA Boost classifier}{10}{subsection.2.3.5}
\contentsline {subsection}{\numberline {2.3.6}Gaussian Naive Bayes}{10}{subsection.2.3.6}
\contentsline {subsection}{\numberline {2.3.7}Quadratic Discrimination Analysis}{10}{subsection.2.3.7}
\contentsline {subsection}{\numberline {2.3.8}Proportional Packet Rate Assumption}{10}{subsection.2.3.8}
\contentsline {paragraph}{Packet Windowing}{10}{section*.17}
\contentsline {section}{\numberline {2.4}Background Information}{11}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Packet capture and analysis}{11}{subsection.2.4.1}
\contentsline {subsubsection}{Wireshark}{11}{section*.18}
\contentsline {subsubsection}{PyShark}{11}{section*.19}
\contentsline {subsubsection}{Jupyter Notebook}{11}{section*.20}
\contentsline {subsubsection}{scikit-learn}{11}{section*.21}
\contentsline {subsection}{\numberline {2.4.2}Datasets}{12}{subsection.2.4.2}
\contentsline {subsubsection}{Benign Datasets}{12}{section*.22}
\contentsline {subsubsection}{Malicious Datasets}{12}{section*.23}
\contentsline {paragraph}{Low Orbit Ion Cannon}{12}{section*.24}
\contentsline {paragraph}{HOIC and XOIC}{12}{section*.25}
\contentsline {paragraph}{UDP Unicorn}{13}{section*.26}
\contentsline {subsubsection}{Outsourced Datasets}{13}{section*.27}
\contentsline {subsubsection}{Other datasets}{13}{section*.28}
\contentsline {subsection}{\numberline {2.4.3}Machine Learning DDoS Detection for Consumer Internet of Things Devices}{13}{subsection.2.4.3}
\contentsline {subsection}{\numberline {2.4.4}Explanation of Confusion Matrices}{14}{subsection.2.4.4}
\contentsline {chapter}{\numberline {3}Analysis of Captures}{15}{chapter.3}
\contentsline {section}{\numberline {3.1}Conversion of Captures}{15}{section.3.1}
\contentsline {section}{\numberline {3.2}Analysis}{15}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}PPRA Investigation Methodology}{15}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Payload Analysis}{18}{subsection.3.2.2}
\contentsline {subsubsection}{LOIC Test Analysis}{18}{section*.34}
\contentsline {subsubsection}{Random Payload Analysis}{18}{section*.36}
\contentsline {subsubsection}{Time Between Packets}{19}{section*.38}
\contentsline {paragraph}{}{19}{section*.39}
\contentsline {chapter}{\numberline {4}Model Creation}{21}{chapter.4}
\contentsline {section}{\numberline {4.1}Feature Selection}{21}{section.4.1}
\contentsline {paragraph}{Payload size}{21}{section*.40}
\contentsline {paragraph}{Time Between Packets}{21}{section*.41}
\contentsline {paragraph}{Protocol}{21}{section*.42}
\contentsline {section}{\numberline {4.2}Training Dataset Creation}{22}{section.4.2}
\contentsline {section}{\numberline {4.3}Model implementation and refinement}{22}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}\(k\)-Nearest Neighbours}{22}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Linear Support Vector}{23}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}RBF Support Vector}{24}{subsection.4.3.3}
\contentsline {subsection}{\numberline {4.3.4}Decision Tree and Random Forest}{25}{subsection.4.3.4}
\contentsline {subsubsection}{Decision Tree}{25}{section*.49}
\contentsline {subsubsection}{Random Forest}{26}{section*.52}
\contentsline {subsection}{\numberline {4.3.5}MLP Classifier}{27}{subsection.4.3.5}
\contentsline {section}{\numberline {4.4}Model Comparison}{28}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Decision Matrix Feature Weight Selection}{29}{subsection.4.4.1}
\contentsline {subsubsection}{Speed}{29}{section*.57}
\contentsline {subsubsection}{Accuracy}{29}{section*.58}
\contentsline {subsubsection}{Weighting calculation}{29}{section*.59}
\contentsline {section}{\numberline {4.5}System Creation}{30}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Model Implementation}{30}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}Initial System - \texttt {LiveCapture Sniff} combination}{30}{subsection.4.5.2}
\contentsline {subsubsection}{System Advantages and Disadvantages}{31}{section*.61}
\contentsline {subsection}{\numberline {4.5.3}Current System - \texttt {apply\_on\_packets}}{31}{subsection.4.5.3}
\contentsline {subsubsection}{System Advantages and Disadvantages}{32}{section*.62}
\contentsline {paragraph}{}{32}{section*.63}
\contentsline {chapter}{\numberline {5}System Analysis}{33}{chapter.5}
\contentsline {section}{\numberline {5.1}Testing Methodology}{33}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Detection Capabilities}{33}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Detection Speeds}{34}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Network Impact}{34}{subsection.5.1.3}
\contentsline {subsection}{\numberline {5.1.4}System Impact}{34}{subsection.5.1.4}
\contentsline {subsection}{\numberline {5.1.5}Test System}{35}{subsection.5.1.5}
\contentsline {section}{\numberline {5.2}Results}{35}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Detection Capabilities}{35}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Detection Speeds}{35}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Network Impact}{36}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}System Impact}{37}{subsection.5.2.4}
\contentsline {subsection}{\numberline {5.2.5}Results Summary}{38}{subsection.5.2.5}
\contentsline {chapter}{\numberline {6}Discussion}{39}{chapter.6}
\contentsline {section}{\numberline {6.1}Stretch Goal - TCP analysis}{39}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Configuration for TCP style attacks}{39}{subsection.6.1.1}
\contentsline {subsubsection}{Dataset creation}{39}{section*.70}
\contentsline {subsubsection}{Model Creation}{40}{section*.71}
\contentsline {subsubsection}{Model Testing}{40}{section*.72}
\contentsline {section}{\numberline {6.2}Model Fit}{40}{section.6.2}
\contentsline {section}{\numberline {6.3}Feature Selection}{41}{section.6.3}
\contentsline {section}{\numberline {6.4}Issues}{41}{section.6.4}
\contentsline {paragraph}{Jupyter Limitations}{41}{section*.73}
\contentsline {paragraph}{Initial System - \texttt {LiveCapture Sniff} combination}{42}{section*.74}
\contentsline {paragraph}{Wireshark Wi-Fi Capabilities}{42}{section*.75}
\contentsline {paragraph}{Non-extensive Protocol Listings}{42}{section*.76}
\contentsline {paragraph}{False Positives - Raising of Termination Flags}{43}{section*.77}
\contentsline {chapter}{\numberline {7}Conclusions}{45}{chapter.7}
\contentsline {chapter}{\numberline {8}Future Work}{47}{chapter.8}
\contentsline {section}{\numberline {8.1}International MultiConference of Engineers and Computer Scientists 2019}{47}{section.8.1}
\contentsline {section}{\numberline {8.2}Future Research Topics}{47}{section.8.2}
\contentsline {subsection}{\numberline {8.2.1}Dataset Creation and Refinement}{48}{subsection.8.2.1}
\contentsline {subsection}{\numberline {8.2.2}Removal of Tshark/Pyshark}{48}{subsection.8.2.2}
\contentsline {subsection}{\numberline {8.2.3}Removal of sci-kit learn}{48}{subsection.8.2.3}
\contentsline {subsection}{\numberline {8.2.4}Inline System Development}{49}{subsection.8.2.4}
\contentsline {chapter}{\numberline {9}Abbreviations}{51}{chapter.9}
\contentsline {chapter}{\numberline {A}Random Datasets}{53}{appendix.A}
\contentsline {chapter}{\numberline {B}Conversion Code for \texttt {.pcapng} files}{55}{appendix.B}
\contentsline {chapter}{\numberline {C}Random Payload Data}{57}{appendix.C}
\contentsline {chapter}{\numberline {D}Research Notebook}{61}{appendix.D}
\contentsline {chapter}{\numberline {E}Model Optimiser Notebook}{101}{appendix.E}
\contentsline {chapter}{\numberline {F}Model Comparison Table}{115}{appendix.F}
\contentsline {chapter}{\numberline {G}Packet Captures}{121}{appendix.G}
\contentsline {chapter}{\numberline {H}Final System - UDP Only}{123}{appendix.H}
\contentsline {chapter}{\numberline {I}Final System - UDP/TCP}{125}{appendix.I}
\contentsline {chapter}{Bibliography}{126}{section*.85}
