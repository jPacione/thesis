import pyshark
import csv
import random

count = 0
with open('GeneratedDataSegmentsSmall10m.csv','w') as file:
    file.write('data,int\n')
    while(count < 10000000):
        print(count)
        count += 1
        randomData = ''
        for x in range(2):#52 for big size 2 for small size
            appendCharacter = random.choice('0123456789abcdef')
            randomData += appendCharacter
        print(randomData)
        print(':'.join(randomData[i:i+2] for i in range(0, len(randomData), 2)))
        scale = 16
        num_of_bits = 8
        binaryStr = bin(int(randomData, scale))[2:].zfill(num_of_bits)
        print(int(binaryStr, 2))
        file.write(':'.join(randomData[i:i+2] for i in range(0, len(randomData), 2)))
        file.write(',')
        file.write(str(int(binaryStr, 2)))
        file.write('\n')
