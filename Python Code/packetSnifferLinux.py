import pyshark
import csv
cap = pyshark.FileCapture('../Captures/donated_capture_jp_1.pcapng', display_filter='udp')

count = 0
with open('donated_capture_jp_1_ints.csv','w') as file:
    file.write('destIP,destPort,srcIP,srcPort,data,int\n')
    for pkt in cap:
        print(count)
        count += 1
        try:
            file.write(pkt.ip.dst)
        except:
            print('dst ip fail')
        file.write(',')
        try:
            file.write(pkt.udp.dstport)
        except:
            print('dst port fail')
        file.write(',')
        try:
            file.write(pkt.ip.src)
        except:
            print('src ip fail')
        file.write(',')
        try:
            file.write(pkt.udp.srcport)
        except:
            print('src port fail')
        file.write(',')
        try:
            payloadStr = pkt.quic.payload
            payloadStr = payloadStr.replace(':','')
            scale = 16
            num_of_bits = 8
            binaryStr = bin(int(payloadStr, scale))[2:].zfill(num_of_bits)
            print(int(binaryStr, 2))
            file.write(pkt.quic.payload)
            file.write(',')
            file.write(str(int(binaryStr, 2)))
        except:
            print('payload fail')
        file.write('\n')
    print('complete')
