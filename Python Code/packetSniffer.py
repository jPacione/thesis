import pyshark
import csv
cap = pyshark.FileCapture('C:\\Users\\jpaci\\Documents\\thesis\\Python Code\\wiresharkCapture1.pcapng', display_filter='udp')
with open('csvfile.csv','w') as file:
    file.write('destination ip, destination port, source ip, source port, data\n')
    for pkt in cap:
        if('GQUIC' not in str(pkt) and 'DNS' not in str(pkt)):
            file.write(pkt.ip.dst)
            file.write(',')
            file.write(pkt.udp.dstport)
            file.write(',')
            file.write(pkt.ip.src)
            file.write(',')
            file.write(pkt.udp.srcport)
            file.write(',')
            file.write(pkt.data.data)
            file.write('\n')