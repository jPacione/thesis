import pyshark
import os
import sys

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import classification_report, confusion_matrix

print('Begin model imports')
maliciousData = pd.read_csv("./randomUDPFlood.csv")
benignData = pd.read_csv(
    "../Captures/donated_capture_jp_3.wireshark_output.csv")
benignData["TBP"] = benignData["Time"] - benignData["Time"].shift()
benignData["Class"] = np.random.randint(1, 2, benignData.shape[0])
columnsTitles = ["Length", "TBP", "Protocol", "Class"]
benignData = benignData.reindex(columns=columnsTitles)
mask = maliciousData.Protocol < 1
column_name = 'Protocol'
maliciousData.loc[mask, column_name] = 'UDP'
frames = [benignData, maliciousData]
modelData = pd.concat(frames)
modelData = modelData.dropna()
global arrayTypes
arrayTypes = modelData['Protocol'].unique()
modelData['Protocol'] = modelData['Protocol'].map(
    lambda x: np.where(arrayTypes == x)[0][0])

X = modelData.iloc[:, :-1].values
y = modelData.iloc[:, 3].values
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20)

print('Begin Model Training')
decisionTree = DecisionTreeClassifier(max_depth=4)
decisionTree.fit(X_train, y_train)
y_pred = decisionTree.predict(X_test)

print('Analysis of Model')
print(confusion_matrix(y_test, y_pred))
print(classification_report(y_test, y_pred))

global iteration
iteration = 0
global benign
benign = 0
global malicious
malicious = 0


def model():
    global arrayTypes
    global iteration
    global benign
    global malicious
    try:
        capture = pyshark.LiveCapture(interface="Wi-Fi", only_summaries=True)
        capture.sniff(packet_count= 50)
        packetInfo = []
        testSet = []
        for pkt in capture:
            packetInfo.append(
                [float(pkt.length), float(pkt.time), pkt.protocol])

        for idx, val in enumerate(packetInfo):
            if(idx != 0):
                if(len(np.where(arrayTypes == val[2])[0]) == 0):
                    arrayTypes = np.append(arrayTypes, val[2])
                testSet.append([val[0], val[1] - packetInfo[idx - 1]
                                [1], np.where(arrayTypes == val[2])[0][0]])

        y_pred = decisionTree.predict(testSet)
        malCount = 0
        for category in y_pred:
            if(category == 1):
                benign += 1
            else:
                malicious += 1
                malCount += 1
        print('Window: '+ str(iteration) + ', Benign: ' + str(benign) + ', Malicious: ' + str(malicious) + ', ' + str(malCount/len(y_pred) * 100), '% malicious')
        if((malCount/len(y_pred) * 100) > 80):
            print('terminate connection here')
        iteration += 1
        capture.close()
    except Exception as e:
        print(e)

while(True):
    model()

